package tech.mastertech.itau.produto.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.produto.dto.Categoria;
import tech.mastertech.itau.produto.dto.Produto;
import tech.mastertech.itau.produto.services.ProdutoService;

@RestController
@RequestMapping("/estoque")
public class EstoqueController {
	
	private Produto retorno;
	
	@Autowired
	private ProdutoService produtoService;
	
//  	@GetMapping("/produtos")
//  	public Iterable<Produto> getProdutos(){
//  		return produtoService.getProdutos();
//  	}
	
  	@PostMapping("/produto")
  	public Produto setProduto(@RequestBody Produto produto) {
  		return produtoService.setProduto(produto);
  	}
  	
  	@GetMapping("produto/{id}")
  	public Produto getProdutoId(@PathVariable int id) {
  		retorno = produtoService.getProdutoId(id);
  		if(retorno == null) {
  			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
  		}
  		return retorno;
  	}
  	
  	@PatchMapping("produto/valor/{id}")
  	public Produto setProdutoValor(@PathVariable String id, @RequestBody String valor) {
  		Produto retorno = produtoService.setProdutoValor(Integer.parseInt(id), Double.parseDouble(valor));
  		if(retorno == null) {
  			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
  		}
  		return retorno;
  	}
  	
  	@GetMapping("produtos")
  	public Iterable<Produto> getProdutos(@RequestParam(required=false) Categoria categoria) {
  		if(categoria == null) {
  		return produtoService.getProdutos();
  		}
  		return produtoService.getProdutos(categoria);
  	}
  	
//	@GetMapping("/hello")
//	public String helloEstoque() {
//		return "HELLO ESTOQUE";
//	}
//	
//	// as chaves permite que a uri entre com uma variável
//	@GetMapping("/repetir/{palavra}")
//	public String repetir(@PathVariable String palavra) {
//		return "<h1> A palavra é " + palavra + "</h1>";
//	}
//	
//	// para inserir o parâmetro na uri, digita ?+nome do parâmetro+=+palavra desejada
//	@GetMapping("/repetir")
//	public String repetirParam(@RequestParam String palavra) {
//		return "A palavra no header é " + palavra;
//	}
//	
//	@PostMapping("/repetir")
//	public String repetirBody(@RequestBody String palavra) {
//		return "A palavra no body é " + palavra;
//	}
}
