package tech.mastertech.itau.produto.dto;

public enum Categoria {
	CONDIMENTOS,
	LIMPEZA,
	ALCOOL,
	LATICINIO;
}
